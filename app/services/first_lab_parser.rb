class FirstLabParser
  require 'htmlentities'
  require 'nokogiri'

  def parse_text_files
    @file_titles = []
    dir_with_files = '/home/dima/parser_files'
    file_names = Dir.entries(dir_with_files).select { |f| f[0] != '.' }
    file_names.each do |file_name|
      collect_file_titles("/home/dima/parser_files/#{file_name}")
    end
    file_names.each do |file_name|
      create_html("/home/dima/parser_files/#{file_name}")
    end
  end

  def get_words_frequency(file_name)
    words_frequency = {}
    content = File.open(file_name, 'r'){ |file| file.read }
    content_without_punctuation_marks = content.gsub(/[.,!?:;]/, '').squish
    content_without_punctuation_marks.split(' ').each_with_index do |word, i|
      if word.length > 3
        words_frequency[word.to_sym] ? words_frequency[word.to_sym] += 1 : words_frequency[word.to_sym] = 1
      end
      if i == 0
        words_frequency[word.to_sym] += 10
      end
    end
    return words_frequency
  end

  def collect_file_titles(file_name)
    words_frequency = get_words_frequency(file_name)
    generate_titles(words_frequency)
  end

  def create_html(file_name)
    content = File.open(file_name, 'r'){ |file| file.read }
    words_frequency = get_words_frequency(file_name)
    xml = generate_xml(words_frequency)
    generate_html(words_frequency, content)
  end

  def generate_xml(words_frequency)
    title = words_frequency.max_by{|k,v| v}.first.to_s
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.frequency {
        words_frequency.each do |word|
          xml.word(repeat_amount: word.last.to_s) {
            xml.text word.first.to_s
          }
        end
      }
    end
    File.write("#{Rails.root}/public/#{title}", HTMLEntities.new.decode(builder.to_xml))
    return title
  end

  def generate_titles(words_frequency)
    title = words_frequency.max_by{|k,v| v}.first.to_s
    @file_titles.push(title) unless @file_titles.include?(title)
  end

  def generate_html(words_frequency, content)
    title = words_frequency.max_by{|k,v| v}.first.to_s
    content = set_links_in_content(content, title)
    file = File.new("#{Rails.root}/public/#{title}.html", "w")
    string = <<-EOF
      <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
      <html>
       <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>#{title}</title>
       </head>
       <body>
        <h1>#{title}</h1>
        <div>#{content}</div>
       </body>
      </html>
    EOF
    file.write string

    file.close()
  end

  def set_links_in_content(content, title)
    updated_content = ""
    content.split(' ').each do |word|
      if @file_titles.include?(word) && word != title
        puts word
        word = "<a href=#{word}.html>#{word}</a>"
      end
      updated_content += "#{word} "
    end
    return updated_content
  end
end