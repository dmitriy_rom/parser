class SecondLabParser
  require 'open-uri'
  require 'nokogiri'
  require 'fileutils'

  def initialize(docs: [], nesting_level: 1, current_nesting_level: 0)
    @docs = docs
    @nesting_level = nesting_level
    @current_nesting_level = current_nesting_level
  end

  def call(url:)
    create_dir("#{Rails.root}/public/images/test.txt")
    fill_docs_with_empty_arrays
    doc = Nokogiri::HTML(URI.open(url))
    create_file(doc: doc)
    while @current_nesting_level < @nesting_level
      @docs[@current_nesting_level].each do |cur_doc|
        collect_nested_files(cur_doc)
      end
      @current_nesting_level += 1
    end
  end

  private

  def create_file(doc:, file_name: 'wiki_article')
    path = "#{Rails.root}/public/#{file_name}.html"
    create_dir(path)
    file = File.new("#{Rails.root}/public/#{file_name}.html", "w")
    @docs[@current_nesting_level + 1].push(doc)
    doc = doc_with_correct_links(doc)
    file.write doc
    file.close()
  end

  def collect_nested_files(doc)
    # Собираем все теги a с текста
    doc.css('a').each do |a_tag|
      href = a_tag[:href]
      # Переходим к следующей ссылке, если у текущей нет урла
      # или это ссылка на FAQ(она сильно тормозит процесс парсинга тк там много ссылок, которые нужно локализовать)
      # или он не начинается с /wiki
      next if href.nil? || href.include?('wiki/Wikipedia:FA') || !href.start_with?('/wiki')

      # Убираем слеш, чтобы он не дублировался
      href[0] = ''
      puts "href: #{href}"
      global_link = "https://en.wikipedia.org/" + href
      puts "global_link: #{global_link}"
      begin
        nested_doc = Nokogiri::HTML(URI.open(global_link))
        create_file(doc: nested_doc, file_name: href)
      rescue => e
        puts "error: #{e}"
      end
    end
  end

  def create_dir(path)
    # Создаем папку, если ее еще нет
    dirname = File.dirname(path)
    unless File.directory?(dirname)
      FileUtils.mkdir_p(dirname)
    end
  end

  def doc_with_correct_links(doc)
    correct_doc = doc.to_s
    doc.css('a').each do |a_tag|
      href = a_tag[:href]
      correct_doc.gsub!(/#{href}/, "/parser/public#{href}.html") if href.present?
    end
    doc.css('img').each do |img_tag|
      src = img_tag[:src]
      file_name = src.split('/').last
      download_image(src, file_name)
      correct_doc.gsub!(/#{src}/, "/parser/public/images/#{file_name}") if src.present?
    end
    return correct_doc
  end

  def fill_docs_with_empty_arrays
    for i in 0..@nesting_level do
      @docs[i] = []
    end
  end

  def download_image(src, file_name)
    begin
      src = 'http:' + src
      open("public/images/#{file_name}", 'wb') do |file|
        file << open(src).read
      end
    rescue => e
      puts e
    end
  end

  def how_to_call
    p = SecondLabParser.new(nesting_level: 2)
    p.call(url: 'https://en.wikipedia.org/wiki/Wikipedia:Very_short_featured_articles')
  end
end